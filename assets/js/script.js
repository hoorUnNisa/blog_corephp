$(document).ready(function () {
    $( "#dateOfBirth" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        placeholder: "Enter Date of Birth",
        maxDate: new Date(1997, 12, 31)
    });
    $( "#dateOfBirth1" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        placeholder: "Enter Date of Birth",
        maxDate: new Date(1997, 12, 31)
    });
    var id;
    $('[data-toggle="tooltip"]').tooltip();
    var myform    = $("#myform");
    var updatebtn = $("#updatebtn");

    myform.submit(function(e) {
        var name      = $("#name").val();
        var email     = $("#email").val();
        var password  = $("#password").val();
        if(name.length >0 && email.length > 0 && password.length > 0 ){
            e.preventDefault();
            $.ajax({
                url: '../process/user.php',
                type: 'POST',
                cache: false,
                data: myform.serialize(),
                success:function(data){
                    console.log(data[0]);
                    if(data[0] == 1 ){
                        $('.emailerror').html("email already exist");

                    }
                    if(data[0]  != 1){
                    $('#myModal').modal('hide');
                    $("#myform").trigger("reset");
                    swal("Registered!", "Your Data is Successfully registered!", "success");
                    }
                }
        });
        }
        else {
            // $('#myModal').modal('hide');
            // $("#myform").trigger("reset");
            // swal("error!", "SomeThing is missing!", "error");
            $('.error').html("Something is missing");
        }
    });


    $("#users-list").change(function(e) {
        e.preventDefault();
        //var id = $(this).val();
        var name = $('#users-list :selected').text();
        id = $('#users-list :selected').val();
        $.ajax({
            //url: "../controller/user.php",
            method: "post",
            data: {id:id,name:name},
            success: function(data){
                //console.log(id);
                $(".user-profile").show();
                $('#userName').text("Hello "+name);

            }
        })
    });

    updatebtn.click(function(e) {
        e.preventDefault();
        var user_id = $(this).attr("id");
        var id = $('#users-list :selected').val();
        console.log(id);
        var action = "updateaction";
        $.ajax({
            url:"../process/user.php",
            method:"POST",
            data:{id:id, action:action},
            dataType:"json",
            success:function(data)
            {
                console.log(data.name);
                $('#name1').val(data.name);
                $('#dateOfBirth1').val(data.dateOfBirth);
                $('#email1').val(data.email);
                $('#address1').val(data.address);
                $('#password1').val(data.password);
            }
        });
    });

    
    


    var upbtn = $("#myform1");
    upbtn.submit(function(e) {
        var id          = $('#users-list :selected').val();
        var name        = $('#name1').val();
        var dateOfBirth = $('#dateOfBirth1').val();
        var email       = $('#email1').val();
        var address     = $('#address1').val();
        var password    = $('#password1').val();
        var action      = "updatedata";
        e.preventDefault();
        if(name.length >0 && email.length > 0 && password.length > 0 && address.length >0 && dateOfBirth.length >0 ) {
            $.ajax({
                url: '../process/user.php',
                type: 'POST',
                data: {
                    id: id,
                    name: name,
                    dateOfBirth: dateOfBirth,
                    email: email,
                    address: address,
                    password: password,
                    action: action
                },
                success: function (data) {
                    $('#myModal1').modal('hide');
                    $("#myform1").trigger("reset");
                    swal("update!", "Your Data is Successfully updated!", "success");
                }
            });
        }
        else{
            $('.error').html("Please fill all the Fields");
        }
    });
    $("#deletebtn").click(function(e) {
        e.preventDefault();
        var id = $('#users-list :selected').val();
        var action = "deletedata";
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this! ",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if(willDelete) {
                $.ajax({
                    url: '../process/user.php',
                    type: 'POST',
                    cache: false,
                    data: {id: id, action: action},
                    success: function (data) {
                        swal("deleted!", "Your Data is Successfully deleted!", "error");
                    }
                });
            }
    })
    });


    $(".comment-form").submit(function(e){
        e.preventDefault();
        var commentForm = $(this);
        var comment     = $(this).children().val();
        var postId      = $(this).children().data("postid");
        console.log(postId);
        var commentType = $(this).children().data("type");
        if (comment.length > 0 && id > 0) {
            $.ajax({
            url: '../process/comment.php',
            type: 'POST',
            data: {commentText : comment , id : postId, type : commentType, userId :id},
            success:function(result){
                if (result == "success") {
                    $.ajax({
                        url: '../process/comment.php',
                        type: 'POST',
                        data: {id : postId, type : "last"},
                        success:function(last){
                            console.log(commentForm.parent().next().append(last));
                        }
                    });
                    swal("Comment!", "Your Comment is Add Successfully!", "success");
                }else{
                    swal("Error!", "There is some problem to add your comment please select your user and try again", "error");
                }
            }
        });
        }else{
            swal("Error!", "There is some problem to add your comment please select your user and try again", "error");
        }
        
    });
    $(document.body).on('click', '.delete-comment',function(){
        var thisDelete = $(this);
        var deleteId = $(this).data("id");
        var deleteComment = "delete";

        swal({
            title: 'Are you sure?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if(willDelete) {
                $.ajax({
                    url: '../process/comment.php',
                    type: 'POST',
                    data: {id: deleteId, type: deleteComment},
                    success: function (result) {
                        if (result == "success") {
                            thisDelete.parent().parent().parent().parent().hide();
                            swal("Comment!", "Your Comment Been Delete Successfully!", "success");
                        } else {
                            swal("Error!", "There is some problem to delete your Comment Please try again", "error");
                        }
                    }
                });
            }
        });

    });


      var liked =0;
      $(".liked").click(function (e) {
            e.preventDefault();
            var userid = $('#users-list :selected').val();

            var post_id =$(this).data('likeid');
            console.log(liked);
            console.log(post_id);
            console.log(userid);
            $.ajax({
                  url: "../process/like.php",
                  type: "POST",
                  data: {post_id : post_id, userId :userid},
                  success: function (data) {
                        $("#like").html(data);
                  }
            });
      });
    //$('#users-list').click(function() {
    $(function () {
        //alert('fdsafsdf');
        var action = "fetchdata";
        $.ajax({
            url: '../process/user.php',
             type: 'POST',
            data:{action:action},
            success: function(result) {
                var data = jQuery.parseJSON(result);
                $.each(data, function(index, value) {
                    if(index>0){
                        $("#users-list").append("<option value="+value.id+">" + value.name + "</option>");
                    }
                });
            }
        });
    });

    $(".edit").click(function(){
        var id = $(this).data("id");
        var id   = $(this).data("id");
        $(".update-comment").click(function(){
            var newComment = $(".update-comment-area").val();

            var po = $("a:first");
            var p = po.offset();
            var pos = new Object();
            pos.left = p.left;
            pos.top  = p.top;
            console.log(p);
            $(".mytext").css("display","block");

            // var myp = $(".mytext").position();
            //console.log(myp);

            if(newComment != "") {

                $.post('../public/UpdateComment.php', {ID: id, updatedComment: newComment}, function (update) {
                    if (update == "updated") {
                        swal("Comment updated", "your comment has been updated successfully");
                        $(".update-comment-box").css("display","none");
                    } else {
                        swal("Error!", "An Error has been occurred while updating your comment. ");
                    }

                });
            } else{

                swal("update fails!","you have not written any new comments so your old comments will be kept.")
                $(".update-comment").parent().parent().hide(parent);
            }
        });

    });
    });
