<?php 
include_once '../modal/DBContaxt.php';
class Comment{
	
	private $_db;
	private $_sql;

	function __construct(){
		$this->_db = new DBContaxt();
	}

	public function insert($text, $id, $userId){
		$this->_sql = "INSERT INTO comments (post_id, user_id, message) VALUES ('$id', '$userId', '$text')";
		$result = $this->_db->insert($this->_sql);
		if ($result) {
			return "Your Comment Add Successfully";
		}
	}

	public function select($post_id){
		$this->_sql = "SELECT comments.message, comments.id AS commentID, users.id AS userId, users.name AS userName,posts.postId AS postId FROM comments INNER JOIN posts ON comments.post_id = posts.postId INNER JOIN users ON comments.user_id = users.id WHERE posts.postId = $post_id";
		$result = $this->_db->select($this->_sql);
		return $result;
	}

    public function selectOne($post_id){
        $this->_sql = "SELECT comments.message, comments.id AS commentID, users.id AS userId, users.name AS userName,posts.postId AS postId FROM comments INNER JOIN posts ON comments.post_id = posts.postId INNER JOIN users ON comments.user_id = users.id WHERE posts.postId = $post_id 
                       ORDER BY comments.id DESC LIMIT 1;";
        $result = $this->_db->select($this->_sql);
        return $result;
    }

	public function update($message, $commentId){
		$this->_sql = "UPDATE comments SET message='$message' WHERE id=$commentId";
		$result = $this->_db->update($this->_sql);
		if ($result) {
			return "Your Comment Update Successfully";
		}
	}

	public function delete($commentId){
		$this->_sql = "DELETE FROM comments WHERE id=$commentId";
		$result = $this->_db->delete($this->_sql);
		if ($result) {
			return "Your Comment Delete Successfully";
		}
	}

}