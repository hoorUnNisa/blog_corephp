<?php
include_once '../modal/DBContaxt.php';

class Post{
	public $_db;
	function __construct()
	{
		$this->_db = new DBContaxt();
	}
    public function add_Post($table,$fields)
    {
        $sql = "";
        $sql .= "INSERT INTO ".$table;
        $sql .= " (".implode(",", array_keys($fields)).") VALUES ";
        $sql .= "('".implode("','", array_values($fields))."')";
        $result = $this->_db->insert($sql);
    }	







    public function fetch_Post()
    {
        $sql = "SELECT users.name, users.id AS userId, posts.postId, posts.postText  FROM posts JOIN users ON users.id=posts.post_userid ORDER BY posts.postId DESC";
        $array = array();
        $query = $this->_db->select($sql);

        // while($row = mysqli_fetch_assoc($query)){
        // 	$array[]=$row;
        // }
        return $query;
    }














    public function select_post($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value) {
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql .="SELECT * FROM ".$table. " WHERE ".$condition;
        //echo $sql;
        $query = $this->_db->select($sql);
        $row = mysqli_fetch_array($query);
        return $row;
    }  


    // public function select_postuser($user_id){
    //     $sql="SELECT users.id AS userId, users.name AS userName,posts.postId AS postId FROM users INNER JOIN posts ON users.id = posts.postId ";
    //     $query = $this->_db->select($sql);
    //     $row = mysqli_fetch_array($query);
    //     return $row;        
    // }


    public function update_post($table,$where,$fields){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value) {
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        foreach ($fields as $key => $value) {
            $sql .= $key . "='".$value."', ";
        }
        $sql = substr($sql, 0, -2);
        $sql ="UPDATE ".$table." SET ".$sql." WHERE ".$condition;
        $query = $this->_db->update($sql);
        if ($query) {
            return true;
        }
    }
    public function delete_post($table,$where){
        $sql = "";
        $condition = "";
        foreach ($where as $key => $value) {
            $condition .= $key . "='" . $value . "' AND ";
        }
        $condition = substr($condition, 0, -5);
        $sql = "DELETE FROM ".$table." WHERE ".$condition;
        $query = $this->_db->delete($sql);
        if ($query) {
            return true;
        }
    }
}
$obj = new Post;
?>
