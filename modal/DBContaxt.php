<?php
class DBContaxt{
    private $_conn;
    private $_myfile;
    private $_dbHost;
    private $_dbUser;
    private $_dbPsw;
    private $_connString;
    public function  __construct(){
        $this->open();
	}

	public function open(){
        $this->_myfile = fopen("../.evn", "r");
        while(!feof($this->_myfile )) {
            $this->_connString .=  fgets($this->_myfile );
        }
        fclose($this->_myfile);

        $connArray = explode("\n",$this->_connString);
        $hostArray = explode("=",$connArray[0]);
        $hostUser  = explode("=",$connArray[1]);
        $hostPsw   = explode("=",$connArray[2]);
        $hostdb   = explode("=",$connArray[3]);

        $this->_dbHost = trim($hostArray[1]);
        $this->_dbUser  = trim($hostUser[1]);
        $this->_dbPsw  = trim($hostPsw[1]);
        $this->_dbName  = trim($hostdb[1]);
        $this->_conn = mysqli_connect("$this->_dbHost", "$this->_dbUser", "$this->_dbPsw","$this->_dbName");

	}

	public function insert($query){
	    $result = mysqli_query($this->_conn,$query);
	    return $result;
	}

	public function select($query){
		$result = mysqli_query($this->_conn,$query);
		return $result;
	}

	public function update($query){
		$result = mysqli_query($this->_conn,$query);
		return $result;
	}

	public function delete($query){
		$result = mysqli_query($this->_conn,$query);
		return $result;
	}

	public function close(){
        mysqli_close($this->_conn);
	}

	public function __destruct(){
        $this->close();
    }

}