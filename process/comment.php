<?php  
	include_once '../controller/comment.php';
	$comment = new comment();
	extract($_POST);
	if ($type == "new") {
		$text = filter_var($commentText, FILTER_SANITIZE_STRING);
		$result = $comment->insert($text, $id, $userId);
		if ($result) {
            echo "success";
        }else{
	        echo "error";
	    }
    }

    if ($type == "delete") {
		$result = $comment->delete($id);
		if ($result) {
            echo "success";
        }else{
	        echo "error";
	    }
    }

    if ($type == "last"){
        $result = $comment->selectOne($id);
        if ($result) {

            if (mysqli_num_rows($result) > 0){
                $rowLast = mysqli_fetch_assoc($result);
                $commentHtml = "<div class='comment-user'>
                                <div class='dropdown pull-right'>
                                        <span class='dropdown-toggle' type='button' data-toggle='dropdown'>
                                            <span class='[ glyphicon glyphicon-chevron-down ] action-post'></span>
                                        </span>
                                    <ul class='dropdown-menu' role='menu'>
                                        <li role='presentation'><a role='menuitem' name='edit' data-id=".$rowLast['commentID']."> Edit</a></li>
                                        <li role='presentation'><a role='menuitem' class='delete-comment'  data-id=".$rowLast['commentID'].">Delete</a></li>
                                    </ul>
                                </div>
                                <img class='comment-user-image' src='../assets/images/user.jpg' alt='User Image'>
                                <span class='comment-user-name'>".$rowLast['userName']."</span>
                                <div class='user-comment-text'>
                                    <p>".$rowLast['message']."</p>
                                </div>
                            </div>";
                echo $commentHtml;
            }
        }else{
            echo "error";
        }
    }

