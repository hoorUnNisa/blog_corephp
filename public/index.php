<?php include "../controller/user.php";

?>
<?php include "../process/post.php";
?>
<?php  include "../controller/Like.php"?>
<html>
<head>
    <title>Blog</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/jquery-ui.css">
    <link rel="stylesheet" href="../assets/css/jquery.fileupload.css">
    <link rel="stylesheet" href="../assets/css/sweetalert.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/check.css">
</head>
<body>
<div class="main-wrapper">
    <div class="container">
        <div class="first-row">
            <div class="col-md-6">
                <select name="" id="users-list" class="form-control pull-left">
                    <option selected disabled>Select Your Self</option>

                </select>
            </div>
            <div class="col-md-6">
                <button class="btn btn-info btn-hover pull-right" data-toggle="modal" data-target="#myModal">
                    Register Your Self
                </button>
            </div>
        </div>
        <div class="main-post-div">
            <div class="col-md-2 col-sm-5 col-xs-12 usersdiv text-center">
                <div class="user-profile ">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img class="comment-user-image" src="../assets/images/user.jpg" alt="User Image">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h4 id="userName"></h4>

                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 butn">
                        <a class="btn btn-primary form-control" id="updatebtn" data-toggle="modal" data-target="#myModal1">Edit Your Self</a>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 butn">
                        <a class="btn btn-danger form-control" id="deletebtn">Delete Your Self</a>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12 post-div">
                <div class="form-div">
                   <div id="t"> 
                    <?php 
                   // if (isset($_GET["pupdate"])) {
                    //var_dump($_GET["pupdate"]);

                        if(isset($_GET["pupdate"])&&($_GET["pupdate"]==true)){
                        if (isset($_GET["postId"])) 
                            $id=$_GET["postId"];
                            $where = array("postId"=>$id,);
                            $row = $obj->select_post("posts",$where);
                           // set($_GET["pupdate"]==false);
                            ?>
                    <form action="" method="post" id="updatepostform">
                        <h4>Update Your Post</h4>
                        <input type="hidden" name="id" id="postid" value="<?php echo $id; ?>">
                        <input type="text" class="form-control" rows="3" id="updatepost" name="postText" placeholder="Enter your text" value="<?php echo $row['postText']; ?>">
                        <div class="form-group">
                            <div id="files" class="files"></div>
                            <div class="col-md-12 padding-non">
                                   <!--  <span class="btn fileinput-button pull-left">
                                        <span class="glyphicon glyphicon-picture" data-toggle="tooltip" title="Add Your Post Image"></span> -->
                                        <!-- The file input field used as target for the file upload widget -->
                                        <!-- <input id="fileupload" name="postImage" type="file" >
                                    </span> -->
                                   <div id="edit-post"> <a class="btn btn-success" role="menuitem" tabindex="-1" id="update" name="update" href="index.php">Update</a></div>


                            </div>
                        </div>
                    </form> 
                    <?php
                 //   }
                    }else{
                        ?>
                    </div>
                    <form action="" method="post" id="addPost">
                        <h4>Create Your Post</h4>
                        <input class="form-control" rows="3" id="posttxt" name="postText" placeholder="Enter your text">
                        <div class="form-group">
                            <div id="files" class="files"></div>
                            <div class="col-md-12 padding-non">
                                   <!--  <span class="btn fileinput-button pull-left">
                                        <span class="glyphicon glyphicon-picture" data-toggle="tooltip" title="Add Your Post Image"></span> -->
                                        <!-- The file input field used as target for the file upload widget -->
                                       <!--  <input id="fileupload" name="postImage" type="file" >
                                    </span> -->
                                <input value="Post" class="btn btn-success pull-right btn-sm post-btn post_btn" id="submit" name="submit" type="submit">
                                    

                            </div>
                        </div>
                    </form>
                        <?php
                    }
                    ?>                    

                </div>
                <div class="post">

                    <div class="">
                        <?php
                            $myrow = $obj->fetch_Post();
                            foreach ($myrow as $row) {
                                
                                ?>
                        <div class="[ panel panel-default ] panel-google-plus post-area">
                            <div class="dropdown">
                                    <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                                        <span class="[ glyphicon glyphicon-chevron-down ] action-post"></span>
                                    </span>
                                <ul class="dropdown-menu" role="menu">

                                    <li role="presentation"><a role="menuitem" id="editpost" tabindex="-1" href="index.php?pupdate=true&postId=<?php echo $row['postId']; ?>">Edit</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?delete=1&postId=<?php echo $row['postId']; ?>">Delete</a></li>
                                </ul>
                            </div>
                            <div class="panel-heading">
                                <img class="[ img-circle pull-left ] comment-user-image" src="../assets/images/user.jpg" alt="Mouse0270" />
                                <h3><?php echo $row["name"]; //include "showUser.php" ?></h3>
                                <h5><span>Shared publicly</span> - <span>Jun 27, 2014</span> </h5>
                            </div>
                            <div class="panel-body">
                                <p>
                                    <?php echo $row["postText"]; ?>
                                        
                                    </p>
                            </div>
                            <div class="panel-footer">
                                <button type="button" class="[ btn btn-default ] like-btn"><span class="glyphicon glyphicon-thumbs-up liked" data-likeid="<?php echo $row['postId']?>"><?php
                                        $object = new Like();
                                        $count="";
                                        $id ="";

                                        if (isset($row['postId']))
                                            $id = $row['postId'];
                                        $query = "SELECT count(*) AS likes from likes WHERE post_id =$id";
                                        $result = $object->_db->select($query);
                                        $ab = mysqli_fetch_array($result);
                                        echo $ab[0];
                                        ?></span></button>
                                <div class="input-placeholder">
                                    <form class="comment-form">
                                        <input type="text" name="formComment" class="form-control" placeholder="Write Your Comment" data-postid="<?php echo $row['postId'] ?>" data-type="new">
                                    </form>
                                </div>
                                <div class="comment-area">
                                    <?php include "showComment.php" ?>
                                </div>
                            </div>
                        </div>                                


                            <?php
                            }

                        ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-main">
            <div class="modal-body form-modal-body">
                <div class="col-md-12">
                    <h3 class="create-heading">Create New Account</h3>

                </div>
                <form class="form-modal"  role="form" id="myform" data-toggle="validator">
                    <div class="col-md-12">
                        <p class="error"></p>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="name">
                            Name*
                        </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Min 3 characters" data-minlength="3" required >
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="dateOfBirth">
                            Date of Birth*
                        </label>
                        <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" required>
                    </div>
                    <div class="col-md-6 form-group">

                        <label for="email">
                            Email*
                        </label>
                        <span class="emailerror"></span>
                        <input type="email" class="form-control" placeholder="Enter your Email" id="email" name="email" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="Passwor">
                            Password*
                        </label>
                        <input type="Password" class="form-control" placeholder="*********" id="password" name="password" required>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="address">
                            Address*
                        </label>
                        <input type="text" class="form-control" placeholder="Address" id="address" name="address" required>
                    </div>

                    <input type="hidden" name="id" id="id" >
                    <input type="hidden" name="regist" id="regist">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="submit-btn" >Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-main">
            <div class="modal-body form-modal-body">
                <div class="col-md-12">
                    <h3 class="create-heading">Update</h3>
                </div>
                <form class="form-modal"  role="form" id="myform1" method="post" data-toggle="validator">
                    <div class="col-md-12">
                        <p class="error"></p>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="name">
                            Name*
                        </label>
                        <input type="text" class="form-control" id="name1" name="name1" placeholder="Min 3 characters" data-minlength="3" required >
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="dateOfBirth">
                            Date of Birth*
                        </label>
                        <input type="text" class="form-control" id="dateOfBirth1" name="dateOfBirth1" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="email">
                            Email*
                        </label>
                        <input type="email" class="form-control" id="email1" name="email1" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="Passwor">
                            Password*
                        </label>
                        <input type="Password" class="form-control" id="password1" name="password1" required>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="address">
                            Address*
                        </label>
                        <input type="text" class="form-control" id="address1" name="address" required>
                    </div>
                    <input type="hidden" name="id1" id="id1">
                    <input type="hidden" name="up" id="up">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="update-btn" value="edit">update</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="myModalupdate" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Your Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="postid1">
                <input type="text" class="form-control" rows="3" id="updatepost1" name="postText" placeholder="Enter your text">
                <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="commentModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content update-comment-box">



            <textarea rows="3" cols="40" class="update-comment-area" placeholder="write new comments or click close to keep the old."></textarea>




            <div class="modal-footer">
                <button name="updateComment" class="update-comment">Update comment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-ui.js"></script>
<script src="../assets/js/jquery.ui.widget.js"></script>
<script src="../assets/js/validator.min.js"></script>
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="../assets/js/jquery.iframe-transport.js"></script>
<script src="../assets/js/jquery.fileupload.js"></script>
<script src="../assets/js/jquery.fileupload-process.js"></script>
<script src="../assets/js/jquery.fileupload-image.js"></script>
<script src="../assets/js/sweetalert.js"></script>
<script src="../assets/js/myfile.js"></script>
<script src="../assets/js/script.js"></script>


<script src="../assets/js/post.js"></script>

</body>
</html>
