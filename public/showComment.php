<?php 
	include_once "../controller/comment.php";
	$comment = new comment();
	$commentResult = $comment->select($row['postId']);
	if (mysqli_num_rows($commentResult) > 0) {
        while ($rowComment = mysqli_fetch_assoc($commentResult)) {

 ?>
<div class="comment-user">

	<div class="dropdown pull-right">
            <span class="dropdown-toggle" type="button" data-toggle="dropdown">
                <span class="[ glyphicon glyphicon-chevron-down ] action-post"></span>
            </span>

        <ul class="dropdown-menu" role="menu">
            <li role="presentation"><a role="menuitem" class="edit" data-id="<?php echo $rowComment['commentID'] ?>" data-toggle="modal" data-target="#commentModal">Edit</a></li>
            <li role="presentation"><a role="menuitem" class="delete-comment"  data-id="<?php echo $rowComment['commentID'] ?>">Delete</a></li>
        </ul>
    </div>

    <img class="comment-user-image" src="../assets/images/user.jpg" alt="User Image">
    <span class="comment-user-name"><?php echo $rowComment['userName']; ?></span>
    <div class="user-comment-text">
        <p class="comment"><?php echo $rowComment['message']; ?></p>
    </div>
</div>
<?php 
        }
    }
 ?>
<?php


?>

